from django.shortcuts import render
from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .models import ExpenseCategory, Account, Receipt
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from receipts.models import Receipt, ExpenseCategory, Account

# Create your views here.


class ReceiptListView(LoginRequiredMixin,CreateView):
    model = Receipt
    template_name = "receipts/list.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]


    # def form_valid(self, form):
    #     form.instance.owner = self.request.user
    #     return super().form_valid(form)
